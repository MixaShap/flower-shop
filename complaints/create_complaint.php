<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Написать жалобу</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
	<div class="container">
  <div class="py-5 text-center">
	<img class="d-block mx-auto mb-4" src="../logo.png" alt="" width="100" height="100">
	<h2>Написать жалобу</h2>
	<p class="lead">Используйте данную форму для того, чтобы написать жалобу</p>
  </div>

  <div class="row">
	<div class="col-md-12 order-md-1">

	  <form name="form_edit_employee" action="create.php" method="post" validate>

<?php
		require_once("../dbconnect.php");

		$result_query = $mysqli->query("SELECT * FROM `сотрудник` WHERE `Работает ли сейчас` = '1' ORDER BY id");
		$employer = mysqli_fetch_assoc($result_query);


		echo "<div class=\"row\">
		  <div class=\"col-md-6 mb-3\">
			<label for=\"seller\">Выберите продавца</label>
			<select class=\"form-control\" name=\"seller\" id=\"seller\">";
		while($sellers = mysqli_fetch_assoc($result_query)) {
				echo "<option value=\"{$sellers['id']}\">{$sellers['Фамилия']} {$sellers['Имя']} {$sellers['Отчество']}</option>";
			}
		echo "</select></div>";


		$result_query = $mysqli->query("SELECT * FROM `постоянный клиент` ORDER BY id");
		echo "
		  <div class=\"col-md-6 mb-3\">
			<label for=\"buyer\">Представьтесь</label>
			<select class=\"form-control\" name=\"buyer\" id=\"buyer\">";
		while($sellers = mysqli_fetch_assoc($result_query)) {
				echo "<option value=\"{$sellers['id']}\">{$sellers['Фамилия']} {$sellers['Имя']} {$sellers['Отчество']}</option>";
			}
		echo "</select></div>";



?>
		<div class="col-md-12 mb-4">
			<label for="complaint">Напишите текст жалобы</label>
			<textarea class="form-control" id="complaint" name="complaint"></textarea>
		</div>

		<hr class="mb-4">
		<button class="btn btn-success btn-lg btn-block" name="btn_recruit" type="submit">Отправить</button>
		<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="../">Отмена</a>
	  </form>
	</div>
  </div>

  <footer class="my-3 pt-3 text-muted text-center text-small">
  </footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"/>
<script src="form-validation.js"/>
</body>
</html>
