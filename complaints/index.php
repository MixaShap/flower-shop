<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ../login', true, 301);
	}
?>

<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Список жалоб</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
	<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

  <a class="navbar-brand navbar col-md-2 col-sm-3 mr-0" href="../">Цветы 🌹</a>

<form class="form-inline my-2 my-lg-0" action="./" method="get">
	  <input class="form-control mr-sm-3" type="text" name="search" placeholder="Поиск по фамилии продавца" aria-label="Search">
	  <button class="btn btn-outline-success my-2 my-sm-0 mr-sm-3" type="submit">Найти!</button>
	  <a class="btn btn-outline-warning my-2 my-sm-0 mr-sm-3" href="./">Сброс</a>
</form>
</nav>


	<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

	  <h2>Жалобы на сотрудников</h2>
	  <div class="table-responsive">
		<table class="table table-striped table-sm">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>Фамилия клиента</th>
			  <th>Имя клиента</th>
			  <th>Отчество клиента</th>
			  <th>ID сотрудника</th>
			  <th>Фамилия сотрудника</th>
			  <th>Имя сотрудника</th>
			  <th>Отчество сотрудника</th>
			  <th>Текст жалобы</th>
			</tr>
		  </thead>
		  <tbody>
			<?php
 
			  setlocale(LC_ALL, "ru_RU");

			  ini_set("display_errors", 1);
			  error_reporting(E_ALL);
			  $name="";
			  if (!empty($_GET['search'])) {
				$name=$_GET['search'];
			  }

			  //Добавляем файл подключения к БД
			  require_once("../dbconnect.php");

			  if ($name=="") {
				$result_query = $mysqli->query("SELECT `жалобы`.`id` AS 'id', `постоянный клиент`.`Фамилия` AS 'Фамилия клиента', `постоянный клиент`.`Имя` AS 'Имя клиента', `постоянный клиент`.`Отчество` AS 'Отчество клиента', `сотрудник`.`id` AS 'ID сотрудника', `сотрудник`.`Фамилия` AS 'Фамилия сотрудника', `сотрудник`.`Имя` AS 'Имя сотрудника', `сотрудник`.`Отчество` AS 'Отчество сотрудника', `Текст жалобы` FROM `жалобы` JOIN `постоянный клиент` ON `жалобы`.`От клиента` = `постоянный клиент`.`id` JOIN `сотрудник` ON `жалобы`.`На сотрудника` = `сотрудник`.`id` ORDER BY `жалобы`.`id`;");
			  } else {
				$result_query = $mysqli->query("SELECT `жалобы`.`id` AS 'id', `постоянный клиент`.`Фамилия` AS 'Фамилия клиента', `постоянный клиент`.`Имя` AS 'Имя клиента', `постоянный клиент`.`Отчество` AS 'Отчество клиента', `сотрудник`.`id` AS 'ID сотрудника', `сотрудник`.`Фамилия` AS 'Фамилия сотрудника', `сотрудник`.`Имя` AS 'Имя сотрудника', `сотрудник`.`Отчество` AS 'Отчество сотрудника', `Текст жалобы` FROM `жалобы` JOIN `постоянный клиент` ON `жалобы`.`От клиента` = `постоянный клиент`.`id` JOIN `сотрудник` ON `жалобы`.`На сотрудника` = `сотрудник`.`id` WHERE `сотрудник`.`Фамилия` LIKE '%".$name."%'  ORDER BY `жалобы`.`id`;");
			  }
			  

			  $result = $result_query->num_rows;

			  while($complaints = mysqli_fetch_assoc($result_query)) {
				  echo "<tr>";
				  echo "<td>{$complaints['id']}</td>\n"; // Вывод

				  echo "<td>{$complaints['Фамилия клиента']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Имя клиента']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Отчество клиента']}</td>\n"; // Вывод
				  echo "<td>{$complaints['ID сотрудника']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Фамилия сотрудника']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Имя сотрудника']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Отчество сотрудника']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Текст жалобы']}</td>\n"; // Вывод

				  echo "</tr>";
			  }
		  
			  $result_query->close();  //очищаем результирующий набор
			?>
		  </tbody>
		</table>
	  </div>
	</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script>
</body>
</html>
