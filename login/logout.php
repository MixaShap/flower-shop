<?php
	session_start();
	ini_set("display_errors",1);
	error_reporting(E_ALL);
	session_unset();
	header('Location: ./', true, 301);
	exit();
?>