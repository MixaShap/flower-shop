<!doctype html>
<html class="" lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Цветы 🌹 Главная</title>

		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="signin.css" rel="stylesheet">
	</head>
	<body class="text-center">
		<form class="form-signin" action="login.php" method="get">
			<img class="mb-4" src="../logo.png" alt="" width="100" height="100">
			<h3 class="h3 mb-3 font-weight-normal">Цветочный магазин</h3>
			<h5 class="h5 mb-3 font-weight-normal">Форма для входа</h5>

			<div class="mb-3">
			  <div class="form-group">
				<label for="login"><b>Логин</b></label>
				<input type="text" class="form-control" name="login" required>
			  </div>
			</div>

			<div class="mb-3">
			  <div class="form-group">
				<label for="passwd"><b>Пароль</b></label>
				<input type="password" class="form-control" name="passwd" required>
			  </div>
			</div>


			<?php
				session_start();
				echo $_SESSION["error_messages"];
			?>

			<button class="btn btn-success btn-lg btn-block" name="btn_recruit" type="submit">Войти</button>
			<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="./login.php?nologin=1">Войти как гость</a>
			<p class="mt-5 mb-3 text-muted">&copy; 2021</p>
		</form>
	</body>
</html>