<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ./login', true, 301);
	}
?>

<!doctype html>
<html class="" lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Цветы 🌹 Главная</title>

		<!-- Bootstrap core CSS -->
		<link href="./css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="signin.css" rel="stylesheet">
	</head>
	<body class="text-center">
		<form class="form-signin">
			<img class="mb-4" src="./logo.png" alt="" width="100" height="100">
			<h3 class="h3 mb-3 font-weight-normal">Цветочный магазин</h3>
			<h5 class="h5 mb-3 font-weight-normal">Главная страница 
			<?php
				if ($_SESSION['login'] != 'guest') {
					echo "менеджера";
				} else {
					echo "покупателя";
				}
			?>
			</h5>
			<?php
				if ($_SESSION['login'] != 'guest') {
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./employees\">Показать список всех сотрудников</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./clients\">Показать список постоянных клиентов</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./complaints\">Показать список жалоб</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./flowers\">Показать список всех цветов</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./shops\">Показать список магазинов</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./buy\">Купить цветы</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./top_buys\">Показать продавцов с наибольшим числом продаж</a>";
				} else {
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./employees\">Показать список всех сотрудников</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./shops\">Показать список магазинов</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./buy\">Купить цветы</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./top_buys\">Показать продавцов с наибольшим числом продаж</a>";
					echo "<a class=\"btn btn-lg btn-primary btn-block\" href=\"./complaints/create_complaint.php\">Написать жалобу</a>";
				}
			?>
			<a class="btn btn-lg btn-warning btn-block" href="./login/logout.php">Выход</a>
			<p class="mt-5 mb-3 text-muted">&copy; 2021</p>
		</form>
	</body>
</html>