-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 20 2021 г., 19:06
-- Версия сервера: 5.7.15
-- Версия PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `flower`
--

-- --------------------------------------------------------

--
-- Структура таблицы `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `passwd` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `accounts`
--

INSERT INTO `accounts` (`id`, `login`, `passwd`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `магазин`
--

CREATE TABLE `магазин` (
  `id` int(11) NOT NULL,
  `Название` varchar(255) NOT NULL,
  `Страна` varchar(255) NOT NULL,
  `Город` varchar(255) NOT NULL,
  `Улица` varchar(255) NOT NULL,
  `Дом` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `магазин`
--

INSERT INTO `магазин` (`id`, `Название`, `Страна`, `Город`, `Улица`, `Дом`) VALUES
(1, 'Первый цветочный', 'Россия', 'Москва', 'Первомайская', 12),
(2, 'Top Shop', 'USA', 'Washington', '8th Avenue', 15);

-- --------------------------------------------------------

--
-- Структура таблицы `доп услуги`
--

CREATE TABLE `доп услуги` (
  `id` int(11) NOT NULL,
  `Название` varchar(255) NOT NULL,
  `Описание` text NOT NULL,
  `Стоимость` int(11) NOT NULL,
  `Дата появления` date NOT NULL,
  `Активно ли предложение` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `должность`
--

CREATE TABLE `должность` (
  `id` int(11) NOT NULL,
  `Название` varchar(255) NOT NULL,
  `Описание` text NOT NULL,
  `Заработная плата` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `корзина`
--

CREATE TABLE `корзина` (
  `id` int(11) NOT NULL,
  `Тип цветка` int(11) NOT NULL,
  `Количество цветков` int(11) NOT NULL,
  `Номер заказа` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `корзина`
--

INSERT INTO `корзина` (`id`, `Тип цветка`, `Количество цветков`, `Номер заказа`) VALUES
(3, 1, 3, 4),
(4, 2, 5, 4),
(5, 1, 1, 5),
(6, 1, 2, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `поставка`
--

CREATE TABLE `поставка` (
  `id` int(11) NOT NULL,
  `Дата поставки` date NOT NULL,
  `Тип цветка` int(11) NOT NULL,
  `Количество цветков` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `постоянный клиент`
--

CREATE TABLE `постоянный клиент` (
  `id` int(11) NOT NULL,
  `Фамилия` varchar(255) NOT NULL,
  `Имя` varchar(255) NOT NULL,
  `Отчество` varchar(255) NOT NULL,
  `Пол` varchar(10) NOT NULL,
  `Дата рождения` date NOT NULL,
  `Страна` varchar(255) NOT NULL,
  `Город` varchar(255) NOT NULL,
  `Улица` varchar(255) NOT NULL,
  `Дом` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `постоянный клиент`
--

INSERT INTO `постоянный клиент` (`id`, `Фамилия`, `Имя`, `Отчество`, `Пол`, `Дата рождения`, `Страна`, `Город`, `Улица`, `Дом`) VALUES
(1, 'Петров', 'Игорь', 'Валерьевич', 'М', '1992-03-15', 'Россия', 'Ростов', 'Ленина', 25),
(2, 'Саравили', 'Дмитрий', 'Альханович', 'М', '1985-05-05', 'Россия', 'Санкт-Петербург', 'Петровская', 75);

-- --------------------------------------------------------

--
-- Структура таблицы `программа лояльности`
--

CREATE TABLE `программа лояльности` (
  `id` int(11) NOT NULL,
  `Пользователь` int(11) NOT NULL,
  `Бонус` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `жалобы`
--

CREATE TABLE `жалобы` (
  `id` int(11) NOT NULL,
  `От клиента` int(11) NOT NULL,
  `На сотрудника` int(11) NOT NULL,
  `Текст жалобы` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `жалобы`
--

INSERT INTO `жалобы` (`id`, `От клиента`, `На сотрудника`, `Текст жалобы`) VALUES
(1, 1, 1, 'Некомпетентность'),
(2, 1, 2, 'Проверочное сообщение');

-- --------------------------------------------------------

--
-- Структура таблицы `заказ`
--

CREATE TABLE `заказ` (
  `id` int(11) NOT NULL,
  `Дата покупки` date NOT NULL,
  `Покупатель` int(11) NOT NULL,
  `Продавец` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `заказ`
--

INSERT INTO `заказ` (`id`, `Дата покупки`, `Покупатель`, `Продавец`) VALUES
(4, '2021-06-20', 2, 3),
(5, '2021-06-20', 1, 1),
(6, '2021-06-20', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `список доп услуг в заказе`
--

CREATE TABLE `список доп услуг в заказе` (
  `id` int(11) NOT NULL,
  `Доп услуга` int(11) NOT NULL,
  `Количество` int(11) NOT NULL,
  `Номер заказа` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `список должностей`
--

CREATE TABLE `список должностей` (
  `id` int(11) NOT NULL,
  `Должность` int(11) NOT NULL,
  `Дата принятия` date NOT NULL,
  `Дата ухода` date NOT NULL,
  `Сотрудник` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `список бонусов`
--

CREATE TABLE `список бонусов` (
  `id` int(11) NOT NULL,
  `Описание бонуса` text NOT NULL,
  `Накопленная сумма от` int(11) NOT NULL,
  `Накопленная сумма до` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `список сотрудников магазина`
--

CREATE TABLE `список сотрудников магазина` (
  `id` int(11) NOT NULL,
  `Сотрудник` int(11) NOT NULL,
  `Магазин` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `сотрудник`
--

CREATE TABLE `сотрудник` (
  `id` int(11) NOT NULL,
  `Фамилия` varchar(255) NOT NULL,
  `Имя` varchar(255) NOT NULL,
  `Отчество` varchar(255) NOT NULL,
  `Пол` varchar(10) NOT NULL,
  `Дата рождения` date NOT NULL,
  `Работает ли сейчас` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `сотрудник`
--

INSERT INTO `сотрудник` (`id`, `Фамилия`, `Имя`, `Отчество`, `Пол`, `Дата рождения`, `Работает ли сейчас`) VALUES
(1, 'Александров', 'Иван', 'Викторович', 'М', '1983-06-14', 1),
(2, 'Иванов', 'Иван', 'Иванович', 'М', '1990-08-25', 1),
(3, 'Хованский', 'Юрий', 'Михайлович', 'М', '1990-01-19', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `цветы`
--

CREATE TABLE `цветы` (
  `id` int(11) NOT NULL,
  `Тип цветка` varchar(255) NOT NULL,
  `Цена за шт` int(11) NOT NULL,
  `Дата появления товара` date NOT NULL,
  `Активно ли предложение` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `цветы`
--

INSERT INTO `цветы` (`id`, `Тип цветка`, `Цена за шт`, `Дата появления товара`, `Активно ли предложение`) VALUES
(1, 'Роза', 80, '2021-06-17', 1),
(2, 'Пион', 56, '2021-06-19', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `магазин`
--
ALTER TABLE `магазин`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `доп услуги`
--
ALTER TABLE `доп услуги`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `должность`
--
ALTER TABLE `должность`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `корзина`
--
ALTER TABLE `корзина`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `Тип цветка` (`Тип цветка`),
  ADD KEY `Номер заказа` (`Номер заказа`);

--
-- Индексы таблицы `поставка`
--
ALTER TABLE `поставка`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Тип цветка` (`Тип цветка`);

--
-- Индексы таблицы `постоянный клиент`
--
ALTER TABLE `постоянный клиент`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `программа лояльности`
--
ALTER TABLE `программа лояльности`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Пользователь` (`Пользователь`),
  ADD KEY `Бонус` (`Бонус`);

--
-- Индексы таблицы `жалобы`
--
ALTER TABLE `жалобы`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `На сотрудника` (`На сотрудника`),
  ADD KEY `От клиента` (`От клиента`);

--
-- Индексы таблицы `заказ`
--
ALTER TABLE `заказ`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `Покупатель` (`Покупатель`),
  ADD KEY `Продавец` (`Продавец`);

--
-- Индексы таблицы `список доп услуг в заказе`
--
ALTER TABLE `список доп услуг в заказе`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Доп услуга` (`Доп услуга`),
  ADD KEY `Номер заказа` (`Номер заказа`);

--
-- Индексы таблицы `список должностей`
--
ALTER TABLE `список должностей`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Должность` (`Должность`),
  ADD KEY `Сотрудник` (`Сотрудник`);

--
-- Индексы таблицы `список бонусов`
--
ALTER TABLE `список бонусов`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `список сотрудников магазина`
--
ALTER TABLE `список сотрудников магазина`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Сотрудник` (`Сотрудник`),
  ADD KEY `Магазин` (`Магазин`);

--
-- Индексы таблицы `сотрудник`
--
ALTER TABLE `сотрудник`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Индексы таблицы `цветы`
--
ALTER TABLE `цветы`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `магазин`
--
ALTER TABLE `магазин`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `корзина`
--
ALTER TABLE `корзина`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `постоянный клиент`
--
ALTER TABLE `постоянный клиент`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `жалобы`
--
ALTER TABLE `жалобы`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `заказ`
--
ALTER TABLE `заказ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `сотрудник`
--
ALTER TABLE `сотрудник`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `цветы`
--
ALTER TABLE `цветы`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `корзина`
--
ALTER TABLE `корзина`
  ADD CONSTRAINT `корзина_ibfk_1` FOREIGN KEY (`Тип цветка`) REFERENCES `цветы` (`id`),
  ADD CONSTRAINT `корзина_ibfk_2` FOREIGN KEY (`Номер заказа`) REFERENCES `заказ` (`id`);

--
-- Ограничения внешнего ключа таблицы `поставка`
--
ALTER TABLE `поставка`
  ADD CONSTRAINT `поставка_ibfk_1` FOREIGN KEY (`Тип цветка`) REFERENCES `цветы` (`id`);

--
-- Ограничения внешнего ключа таблицы `программа лояльности`
--
ALTER TABLE `программа лояльности`
  ADD CONSTRAINT `программа лояльности_ibfk_1` FOREIGN KEY (`Пользователь`) REFERENCES `постоянный клиент` (`id`),
  ADD CONSTRAINT `программа лояльности_ibfk_2` FOREIGN KEY (`Бонус`) REFERENCES `список бонусов` (`id`);

--
-- Ограничения внешнего ключа таблицы `жалобы`
--
ALTER TABLE `жалобы`
  ADD CONSTRAINT `жалобы_ibfk_1` FOREIGN KEY (`На сотрудника`) REFERENCES `сотрудник` (`id`),
  ADD CONSTRAINT `жалобы_ibfk_2` FOREIGN KEY (`От клиента`) REFERENCES `постоянный клиент` (`id`);

--
-- Ограничения внешнего ключа таблицы `заказ`
--
ALTER TABLE `заказ`
  ADD CONSTRAINT `заказ_ibfk_1` FOREIGN KEY (`Покупатель`) REFERENCES `постоянный клиент` (`id`),
  ADD CONSTRAINT `заказ_ibfk_2` FOREIGN KEY (`Продавец`) REFERENCES `сотрудник` (`id`);

--
-- Ограничения внешнего ключа таблицы `список доп услуг в заказе`
--
ALTER TABLE `список доп услуг в заказе`
  ADD CONSTRAINT `список доп услуг в заказе_ibfk_1` FOREIGN KEY (`Доп услуга`) REFERENCES `доп услуги` (`id`),
  ADD CONSTRAINT `список доп услуг в заказе_ibfk_2` FOREIGN KEY (`Номер заказа`) REFERENCES `заказ` (`id`);

--
-- Ограничения внешнего ключа таблицы `список должностей`
--
ALTER TABLE `список должностей`
  ADD CONSTRAINT `список должностей_ibfk_1` FOREIGN KEY (`Должность`) REFERENCES `должность` (`id`),
  ADD CONSTRAINT `список должностей_ibfk_2` FOREIGN KEY (`Сотрудник`) REFERENCES `сотрудник` (`id`);

--
-- Ограничения внешнего ключа таблицы `список сотрудников магазина`
--
ALTER TABLE `список сотрудников магазина`
  ADD CONSTRAINT `список сотрудников магазина_ibfk_1` FOREIGN KEY (`Сотрудник`) REFERENCES `сотрудник` (`id`),
  ADD CONSTRAINT `список сотрудников магазина_ibfk_2` FOREIGN KEY (`Магазин`) REFERENCES `магазин` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
