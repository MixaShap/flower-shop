<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ../login', true, 301);
	}
?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<title>Цветы 🌹 Покупка цветов</title>

	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
		  		font-size: 3.5rem;
			}
		}
	</style>
	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
	</head>
	<body>
		<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

			<a class="navbar-brand navbar col-md-2 col-sm-3 mr-0" href="../">Цветы 🌹</a>

			<form class="form-inline my-2 my-lg-0" action="./" method="get">
				<input class="form-control mr-sm-3" type="text" name="search" placeholder="Поиск по названию" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0 mr-sm-3" type="submit">Найти!</button>
				<a class="btn btn-outline-warning my-2 my-sm-0 mr-sm-3" href="./">Сброс</a>
			</form>
		</nav>


		<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

			<h2>Цветы</h2>
			<div class="table-responsive">
				<form name="form_buy" action="add_order.php" method="get" >
					<table class="table table-striped table-sm">
						<thead>
							<tr>
								<th>#</th>
								<th>Тип цветка</th>
								<th>Цена за шт</th>
								<th>Количество</th>
								<th>Сумма</th>
							</tr>
						</thead>
						<tbody>
							<?php
				 
								setlocale(LC_ALL, "ru_RU");

								ini_set("display_errors", 1);
								error_reporting(E_ALL);
								$name="";
								if (!empty($_GET['search'])) {
									$name=$_GET['search'];
								}

								//Добавляем файл подключения к БД
								require_once("../dbconnect.php");

								if ($name=="") {
									$result_query = $mysqli->query("SELECT * FROM `цветы` WHERE `Активно ли предложение` = '1' ORDER BY id");
								} else {
									$result_query = $mysqli->query("SELECT * FROM `цветы` WHERE `Активно ли предложение` = '1' AND `Тип цветка` LIKE '%".$name."%' ORDER BY id");
								}
							  

								$result = $result_query->num_rows;

								while($buy_flowers = mysqli_fetch_assoc($result_query)) {
									echo "<tr>";
									echo "<td>{$buy_flowers['id']}</td>\n"; // Вывод

									echo "<td>{$buy_flowers['Тип цветка']}</td>\n"; // Вывод
									echo "<td>{$buy_flowers['Цена за шт']} ₽</td>\n"; // Вывод

									echo "<td><input type=\"number\" size=\"3\" id=\"amount_{$buy_flowers['id']}\" name=\"amount_{$buy_flowers['id']}\" min=\"0\" max=\"10\" value=\"0\"></td>";

									echo "<td id =\"price_{$buy_flowers['id']}\">0
											<script>
												var amountField_{$buy_flowers['id']} = document.getElementById('amount_{$buy_flowers['id']}');
												var costField_{$buy_flowers['id']} = document.getElementById('cost_{$buy_flowers['id']}');
												var priceCount_{$buy_flowers['id']} = document.getElementById('price_{$buy_flowers['id']}');

												amountField_{$buy_flowers['id']}.onkeyup = getValueOfInputNumber;
												amountField_{$buy_flowers['id']}.onchange = getValueOfInputNumber;

												function getValueOfInputNumber() {
														priceCount_{$buy_flowers['id']}.innerHTML = this.value * {$buy_flowers['Цена за шт']};
														count_summ();
												}
											</script>
								  		</td>";

									echo "</tr>";
								}
								
								echo "<tr>";
								echo "<td></td>\n";
								echo "<td></td>\n";
								echo "<td></td>\n";
								echo "<td align=\"right\"><b>Итого: </b></td>\n";
								echo "<td><b id=\"summ\">0
									<script>
										var summField = document.getElementById('summ');\n";

								for ($i=1; $i <= $result_query->num_rows; $i++) { 
									echo "var summ{$i} = document.getElementById('price_{$i}');\n";
									echo "summ{$i}.onchange = count_summ;\n";
									echo "summ{$i}.addEventListener('change', count_summ);\n\n";
								}

								echo "
										function count_summ() {
											summField.innerHTML = ";

								for ($i=1; $i <= $result_query->num_rows; $i++) { 
									echo "parseInt(summ{$i}.innerHTML)";
									if ($i != $result_query->num_rows) {
										echo " + ";
									}
									
								}				

								echo ";\n	}
									</script>
								</b></td>\n";
								echo "</tr>";

								echo "<input type=\"hidden\" name=\"size\" id=\"size\" value=\"{$result_query->num_rows}\">";

								$result_query->close();  //очищаем результирующий набор
							?>
						</tbody>
					</table>
	    			<button class="btn btn-primary btn-lg btn-block" name="btn_buy" type="submit">Продолжить</button>
	    		</form>
			</div>
		</main>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
		<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="../js/bootstrap.bundle.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
		<script src="dashboard.js"></script>
	</body>
</html>
