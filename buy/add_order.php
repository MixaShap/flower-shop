<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<title>Цветы 🌹 Оформление заказа</title>

		<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


		<style>
			.bd-placeholder-img {
				font-size: 1.125rem;
				text-anchor: middle;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}

			@media (min-width: 768px) {
				.bd-placeholder-img-lg {
					font-size: 3.5rem;
				}
			}
		</style>
		<!-- Custom styles for this template -->
		<link href="form-validation.css" rel="stylesheet">
	</head>
	<body class="bg-light">
		<div class="container">
	<div class="py-5 text-center">
		<img class="d-block mx-auto mb-4" src="../logo.png" alt="" width="100" height="100">
		<h2>Оформление заказа</h2>
		<p class="lead">Используйте данную форму для оформления заказа</p>
	</div>

	<div class="row">
		<div class="col-md-12 order-md-1">
			<h4 class="mb-3">Укажите данные:</h4>

			<form name="form_flowers" action="add.php" method="get" validate>

				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="name">Продавец</label>
						<select class="form-control" name="seller" id="seller">
							<?php
								setlocale(LC_ALL, "ru_RU");

								ini_set("display_errors", 1);
								error_reporting(E_ALL);

								//Добавляем файл подключения к БД
								require_once("../dbconnect.php");

								$result_query = $mysqli->query("SELECT * FROM `сотрудник` WHERE `Работает ли сейчас` = '1' ORDER BY id");
								$result = $result_query->num_rows;

								while($sellers = mysqli_fetch_assoc($result_query)) {
									echo "<option value=\"{$sellers['id']}\">{$sellers['Фамилия']} {$sellers['Имя']} {$sellers['Отчество']}</option>";
								}
							?>
						</select>
					</div>
					<div class="col-md-6 mb-3">
						<label for="name">Покупатель</label>
						<select class="form-control" name="buyer" id="buyer">
							<?php
								$result_query = $mysqli->query("SELECT * FROM `постоянный клиент` ORDER BY id");
								$result = $result_query->num_rows;

								while($buyers = mysqli_fetch_assoc($result_query)) {
									echo "<option value=\"{$buyers['id']}\">{$buyers['Фамилия']} {$buyers['Имя']} {$buyers['Отчество']}</option>";
								}
							?>
						</select>
					</div>
				</div>

				<?php
					for ($i=1; $i <= $_GET['size']; $i++) { 
						$value = 'amount_'.$i;
						echo "<input type=\"hidden\" name=\"amount_{$i}\" id=\"amount_{$i}\" value=\"{$_GET[$value]}\">";
					}
					echo "<input type=\"hidden\" name=\"size\" id=\"size\" value=\"{$_GET['size']}\">";
				?>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" name="btn_recruit" type="submit">Оформить заказ</button>
				<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="./">Назад</a>
			</form>
		</div>
	</div>

	<footer class="my-3 pt-3 text-muted text-center text-small">
	</footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"/>
<script src="form-validation.js"/>
</body>
</html>
