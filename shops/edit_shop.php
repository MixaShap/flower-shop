<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Редактирование данных о магазине</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
	<div class="container">
  <div class="py-5 text-center">
	<img class="d-block mx-auto mb-4" src="../logo.png" alt="" width="100" height="100">
	<h2>Редактирование данных о магазине</h2>
	<p class="lead">Используйте данную форму для редактирования данных о магазине</p>
  </div>

  <div class="row">
	<div class="col-md-12 order-md-1">
	  <h4 class="mb-3">Данные магазина:</h4>

	  <form name="form_edit_shop" action="edit.php" method="post" validate>

<?php
		require_once("../dbconnect.php");

		$result_query = $mysqli->query("SELECT * FROM `магазин` WHERE id=".$_GET['id']);
		$result = $result_query->num_rows;
		$shop = mysqli_fetch_assoc($result_query);

		echo "<div class=\"row\">
			<div class=\"col-md-6 mb-3\">
				<label for=\"name\">Название</label>
				<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\"  value='".$shop['Название']."' required>
			</div>";

		echo "<div class=\"col-md-6 mb-3\">
			<label for=\"country\">Страна</label>
			<input type=\"text\" class=\"form-control\" name=\"country\" id=\"country\" value='".$shop['Страна']."' required>
		  </div>
		</div>


		<div class=\"row\">
			<div class=\"col-md-6 mb-3\">
			  <label for=\"city\">Город</label>
			  <input type=\"text\" class=\"form-control\" name=\"city\" id=\"city\" value='".$shop['Город']."' required>
			</div>


			<div class=\"col-md-6 mb-3\">
			  <div class=\"form-group\">
				<label for=\"street\">Улица</label>
				<input type=\"text\" class=\"form-control\" id=\"street\" name=\"street\" value='".$shop['Улица']."' required>
			  </div>
			</div>
		</div>


		<div class=\"row\">
			<div class=\"col-md-6 mb-3\">
				<label for=\"house\">Дом</label>
				<input type=\"text\" class=\"form-control\" name=\"house\" id=\"house\" value='".$shop['Дом']."' required>
		  	</div>

			<div class=\"col-md-6 mb-3\">
				<label for=\"deleted\">Удалить?</label>

				<input class=\"btn btn-sm btn-danger form-control\" id=\"btn_deleted{$shop['id']}\" type=\"button\" value=\"Удалить магазин\"/>
					  <script>
						btn_deleted{$shop['id']}.onclick = function() {
						  var isFired = confirm(\"Вы действительно хотите удалить магазин {$shop['Название']}?\");
						  if (isFired){
							window.location.href = \"./deleted.php?id={$shop['id']}\";
						  }
						};
					  </script>
			</div>
		</div>

		<input type=\"hidden\" class=\"form-control\" name=\"id\" id=\"id\" value=".$_GET['id'].">
		";

?>

		<hr class="mb-4">
		<button class="btn btn-success btn-lg btn-block" name="btn_recruit" type="submit">Сохранить изменения!</button>
		<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="./">Отмена</a>
	  </form>
	</div>
  </div>

  <footer class="my-3 pt-3 text-muted text-center text-small">
  </footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"/>
<script src="form-validation.js"/>
</body>
</html>
