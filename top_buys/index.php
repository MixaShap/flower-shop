<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ../login', true, 301);
	}
?>

<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Наибольшие продажи</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
	<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

  <a class="navbar-brand navbar col-md-2 col-sm-3 mr-0" href="../">Цветы 🌹</a>
</nav>


	<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

	  <h2>Наибольшие продажи</h2>
	  <div class="table-responsive">
		<table class="table table-striped table-sm">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>Фамилия продавца</th>
			  <th>Имя продавца</th>
			  <th>Отчество продавца</th>
			  <th>Количество заказов</th>
			</tr>
		  </thead>
		  <tbody>
			<?php
 
			  setlocale(LC_ALL, "ru_RU");

			  ini_set("display_errors", 1);
			  error_reporting(E_ALL);
			  $name="";
			  if (!empty($_GET['search'])) {
				$name=$_GET['search'];
			  }

			  //Добавляем файл подключения к БД
			  require_once("../dbconnect.php");

				$result_query = $mysqli->query("SELECT `заказ`.`Продавец`, `сотрудник`.`Фамилия`, `сотрудник`.`Имя`, `сотрудник`.`Отчество`, COUNT(`заказ`.`Продавец`) AS 'Число заказов' FROM `заказ` JOIN `сотрудник` ON `заказ`.`Продавец` = `сотрудник`.`id` GROUP BY `заказ`.`Продавец` ORDER BY `заказ`.`Продавец` LIMIT 10;");
			  
			  

			  $result = $result_query->num_rows;

			  while($complaints = mysqli_fetch_assoc($result_query)) {
				  echo "<tr>";
				  echo "<td>{$complaints['Продавец']}</td>\n"; // Вывод

				  echo "<td>{$complaints['Фамилия']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Имя']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Отчество']}</td>\n"; // Вывод
				  echo "<td>{$complaints['Число заказов']}</td>\n"; // Вывод

				  echo "</tr>";
			  }
		  
			  $result_query->close();  //очищаем результирующий набор
			?>
		  </tbody>
		</table>
	  </div>
	</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script>
</body>
</html>
