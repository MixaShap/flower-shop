<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<title>Цветы 🌹 Добавление цветов</title>

		<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


		<style>
			.bd-placeholder-img {
				font-size: 1.125rem;
				text-anchor: middle;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}

			@media (min-width: 768px) {
				.bd-placeholder-img-lg {
					font-size: 3.5rem;
				}
			}
		</style>
		<!-- Custom styles for this template -->
		<link href="form-validation.css" rel="stylesheet">
	</head>
	<body class="bg-light">
		<div class="container">
	<div class="py-5 text-center">
		<img class="d-block mx-auto mb-4" src="../logo.png" alt="" width="100" height="100">
		<h2>Добавление цветов</h2>
		<p class="lead">Используйте данную форму для добавления цветов</p>
	</div>

	<div class="row">
		<div class="col-md-12 order-md-1">
			<h4 class="mb-3">Данные цветов:</h4>

			<form name="form_flowers" action="add.php" method="post" validate>

				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="name">Тип цветка</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Роза" value="" required>
					</div>
					<div class="col-md-6 mb-3">
						<label for="price">Цена за шт в рублях</label>
						<input type="text" class="form-control" name="price" id="price" placeholder="80" value="" required>
					</div>
				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" name="btn_recruit" type="submit">Добавить цветы</button>
				<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="./">Назад</a>
			</form>
		</div>
	</div>

	<footer class="my-3 pt-3 text-muted text-center text-small">
	</footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"/>
<script src="form-validation.js"/>
</body>
</html>
