<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Редактирование данных о цветах</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
	<div class="container">
  <div class="py-5 text-center">
	<img class="d-block mx-auto mb-4" src="../logo.png" alt="" width="100" height="100">
	<h2>Редактирование данных о цветах</h2>
	<p class="lead">Используйте данную форму для редактирования данных о цветах</p>
  </div>

  <div class="row">
	<div class="col-md-12 order-md-1">
	  <h4 class="mb-3">Данные цветов:</h4>

	  <form name="form_edit_flowers" action="edit.php" method="post" validate>

<?php
		require_once("../dbconnect.php");

		$result_query = $mysqli->query("SELECT * FROM `цветы` WHERE id=".$_GET['id']);
		$result = $result_query->num_rows;
		$flower = mysqli_fetch_assoc($result_query);
		$sold_out = "unchecked";

		if($flower['Активно ли предложение'] == 0)
			$sold_out = "checked";

		echo "<div class=\"row\">
		  <div class=\"col-md-6 mb-3\">
			<label for=\"name\">Тип цветка</label>
			<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" value=".$flower['Тип цветка']." required>
		  </div>";

		echo "<div class=\"col-md-6 mb-3\">
			<label for=\"price\">Цена за шт</label>
			<input type=\"text\" class=\"form-control\" name=\"price\" id=\"price\" value=".$flower['Цена за шт']." required>
		  </div>
		</div>

		<div class=\"row\">

			<div class=\"col-md-6 mb-3\">
				<div class=\"form-group\">
					<label for=\"flower_date_add\">Дата появления товара</label>
					<input type=\"date\" class=\"form-control\" name=\"flower_date_add\" id=\"flower_date_add\" value=".$flower['Дата появления товара']." required>
				</div>
			</div>

			<input type=\"hidden\" class=\"form-control\" name=\"sold_out\" id=\"sold_out\" value=off>
			<div class=\"col-md-6 mb-3\">
				<label for=\"sold_out\">Распродан?</label>
				<input type=\"checkbox\" class=\"form-control\" name=\"sold_out\" id=\"sold_out\" ".$sold_out.">
			</div>
		</div>

		<input type=\"hidden\" class=\"form-control\" name=\"id\" id=\"id\" value=".$_GET['id'].">
		";

?>

		<hr class="mb-4">
		<button class="btn btn-success btn-lg btn-block" name="btn_recruit" type="submit">Сохранить изменения!</button>
		<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="./">Отмена</a>
	  </form>
	</div>
  </div>

  <footer class="my-3 pt-3 text-muted text-center text-small">
  </footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"/>
<script src="form-validation.js"/>
</body>
</html>
