<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ../login', true, 301);
	}

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
	header("Cache-Control: no-store, no-cache, must-revalidate"); 
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>

<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Список цветов</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
	<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

  <a class="navbar-brand navbar col-md-2 col-sm-3 mr-0" href="../">Цветы 🌹</a>

<form class="form-inline my-2 my-lg-0" action="./" method="get">
	  <input class="form-control mr-sm-3" type="text" name="search" placeholder="Поиск по названию" aria-label="Search">
	  <button class="btn btn-outline-success my-2 my-sm-0 mr-sm-3" type="submit">Найти!</button>
	  <a class="btn btn-outline-warning my-2 my-sm-0 mr-sm-3" href="./">Сброс</a>
</form>
</nav>

<div class="container-fluid">
  <div class="row">
	<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	  <div class="sidebar-sticky">
		<ul class="nav flex-column">
		  <li class="nav-item">
			<a class="nav-link active" href="">
			  <span data-feather="users"></span>
			  Список цветов<span class="sr-only">(current)</span>
			</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="./add_flower.php">
			  <span data-feather="user-plus"></span>
			  Добавить цветы
			</a>
		  </li>
		</ul>
	  </div>
	</nav>

	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

	  <h2>Цветы</h2>
	  <div class="table-responsive">
		<table class="table table-striped table-sm">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>Тип цветка</th>
			  <th>Цена за шт</th>
			  <th>Дата появления товара</th>
			  <th>Редактировать</th>
			  <th>Активно ли предложение</th>
			</tr>
		  </thead>
		  <tbody>
			<?php
 
			  setlocale(LC_ALL, "ru_RU");

			  ini_set("display_errors", 1);
			  error_reporting(E_ALL);
			  $name="";
			  if (!empty($_GET['search'])) {
				$name=$_GET['search'];
			  }

			  //Добавляем файл подключения к БД
			  require_once("../dbconnect.php");

			  if ($name=="") {
				$result_query = $mysqli->query("SELECT * FROM `цветы` ORDER BY id");
			  } else {
				$result_query = $mysqli->query("SELECT * FROM `цветы` WHERE `Тип цветка` LIKE '%".$name."%' ORDER BY id");
			  }
			  

			  $result = $result_query->num_rows;

			  while($flower = mysqli_fetch_assoc($result_query)) {
				  echo "<tr>";
				  echo "<td>{$flower['id']}</td>\n"; // Вывод

				  echo "<td>{$flower['Тип цветка']}</td>\n"; // Вывод
				  echo "<td>{$flower['Цена за шт']} ₽</td>\n"; // Вывод
				  echo "<td>{$flower['Дата появления товара']}</td>\n"; // Вывод


				  echo "<td><input class=\"btn btn-sm btn-success btn-block\" id=\"btn_edit{$flower['id']}\" type=\"button\" value=\"✏️\"/>
					  <script>
						btn_edit{$flower['id']}.onclick = function() {
						  window.location.href = \"./edit_flower.php?id={$flower['id']}\";
						};
					  </script>
					</td>";


				  if ($flower['Активно ли предложение'] == 1) {
					echo "<td><input class=\"btn btn-sm btn-danger btn-block\" id=\"btn_fired{$flower['id']}\" type=\"button\" value=\"Снять с продажи\"/>
					  <script>
						btn_fired{$flower['id']}.onclick = function() {
						  var isFired = confirm(\"Вы действительно хотите снять с продажи #{$flower['id']} {$flower['Тип цветка']}?\");
						  if (isFired){
						  	// alert(\"./fired.php?id={$flower['id']}\");
							window.location.href = \"./sold.php?id={$flower['id']}\";
						  }
						};
					  </script>
					</td>";
				  } else {
					echo "<td>Товар распродан</td>"; // Вывод
				  }
				  echo "</tr>";
			  }
		  
			  $result_query->close();  //очищаем результирующий набор
			?>
		  </tbody>
		</table>
	  </div>
	</main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script>
</body>
</html>
