<!doctype html>
<html class="" lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Цветы 🌹 Главная</title>

		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="signin.css" rel="stylesheet">
	</head>
	<body class="text-center">
		<form class="form-signin" action="login.php" method="get">
			<img class="mb-4" src="../logo.png" alt="" width="100" height="100">
			<h3 class="h3 mb-3 font-weight-normal">Цветочный магазин</h3>
			<h4 class="h5 mb-3 font-weight-normal">У вас нет доступа к данной странице</h4>
			<?php
				session_start();
				echo $_SESSION["error_messages"];
			?>
			<a class="btn btn-warning btn-lg btn-block" name="btn_cancel" href="../">На главную</a>
			<p class="mt-5 mb-3 text-muted">&copy; 2021</p>
		</form>
	</body>
</html>