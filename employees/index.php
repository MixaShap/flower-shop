<?php
	session_start();
	if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
		header('Location: ../login', true, 301);
	}

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title>Цветы 🌹 Сотрудники</title>

	<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


	<style>
	  .bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	  }

	  @media (min-width: 768px) {
		.bd-placeholder-img-lg {
		  font-size: 3.5rem;
		}
	  }
	</style>
	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
	<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

  <a class="navbar-brand navbar col-md-2 col-sm-3 mr-0" href="../">Цветы 🌹</a>

<form class="form-inline my-2 my-lg-0" action="./" method="get">
	  <input class="form-control mr-sm-3" type="text" name="search" placeholder="Поиск по фамилии" aria-label="Search">
	  <button class="btn btn-outline-success my-2 my-sm-0 mr-sm-3" type="submit">Найти!</button>
	  <a class="btn btn-outline-warning my-2 my-sm-0 mr-sm-3" href="./">Сброс</a>
</form>
</nav>

<div class="container-fluid">
  <div class="row">
	<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	  <div class="sidebar-sticky">
		<ul class="nav flex-column">
		  <li class="nav-item">
			<a class="nav-link active" href="">
			  <span data-feather="users"></span>
			  Список сотрудников<span class="sr-only">(current)</span>
			</a>
		  </li>

			<?php
				if ($_SESSION['login'] != 'guest') {
					echo "<li class=\"nav-item\">
							<a class=\"nav-link\" href=\"./add_employer.php\">
			  				<span data-feather=\"user-plus\"></span>
			  					Нанять сотрудника
							</a>
		 				</li>";
				}
			?>
		</ul>
	  </div>
	</nav>

	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

	  <h2>Сотрудники</h2>
	  <div class="table-responsive">
		<table class="table table-striped table-sm">
		  <thead>
			<tr>
				<th>#</th>
				<th>Фамилия</th>
				<th>Имя</th>
				<th>Отчество</th>
				<th>Пол</th>
				<th>Дата рождения</th>


				<?php
					if ($_SESSION['login'] != 'guest') {
						echo "<th>Редактировать</th>
						  	<th>Работает ли сейчас</th>";
					}
				?>
			</tr>
		  </thead>
		  <tbody>
			<?php
 
			  setlocale(LC_ALL, "ru_RU");

			  ini_set("display_errors", 1);
			  error_reporting(E_ALL);
			  $surname="";
			  if (!empty($_GET['search'])) {
				$surname=$_GET['search'];
			  }

			  //Добавляем файл подключения к БД
			  require_once("../dbconnect.php");

			  if ($surname=="") {
				$result_query = $mysqli->query("SELECT * FROM `сотрудник` ORDER BY id");
			  } else {
				$result_query = $mysqli->query("SELECT * FROM `сотрудник` WHERE `Фамилия` LIKE '%".$surname."%' ORDER BY id");
			  }
			  

				$result = $result_query->num_rows;

				while($employer = mysqli_fetch_assoc($result_query)) {
					echo "<tr>";
					echo "<td>{$employer['id']}</td>\n"; // Вывод

					echo "<td>{$employer['Фамилия']}</td>\n"; // Вывод
					echo "<td>{$employer['Имя']}</td>\n"; // Вывод
					echo "<td>{$employer['Отчество']}</td>\n"; // Вывод
					echo "<td>{$employer['Пол']}</td>\n"; // Вывод
					echo "<td>{$employer['Дата рождения']}</td>\n"; // Вывод

					if ($_SESSION['login'] != 'guest') {
						echo "<td><input class=\"btn btn-sm btn-success btn-block\" id=\"btn_edit{$employer['id']}\" type=\"button\" value=\"✏️\"/>
							<script>
								btn_edit{$employer['id']}.onclick = function() {
									window.location.href = \"./edit_employer.php?id={$employer['id']}\";
								};
						  	</script>
							</td>";
					}

				  if ($_SESSION['login'] != 'guest') {
				  if ($employer['Работает ли сейчас'] == 1) {
					echo "<td><input class=\"btn btn-sm btn-danger btn-block\" id=\"btn_fired{$employer['id']}\" type=\"button\" value=\"Уволить!\"/>
					  <script>
						btn_fired{$employer['id']}.onclick = function() {
						  var isFired = confirm(\"Вы действительно хотите уволить сотрудника {$employer['Фамилия']} {$employer['Имя']} {$employer['Отчество']}?\");
						  if (isFired){
							// alert(\"./fired.php?id={$employer['id']}\");
							window.location.href = \"./fired.php?id={$employer['id']}\";
						  }
						};
					  </script>
					</td>";
				  } else {
					echo "<td>Уволен</td>"; // Вывод
				  }
				}
				  echo "</tr>";
			  }
		  
			  $result_query->close();  //очищаем результирующий набор
			?>
		  </tbody>
		</table>
	  </div>
	</main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script>
</body>
</html>
